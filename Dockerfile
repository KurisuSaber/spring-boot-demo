# build stage
FROM maven:3.5.2-jdk-8-alpine as build-stage
COPY . .
RUN mvn clean package

# production stage
FROM openjdk:8 as production-stage
COPY --from=build-stage ./target/demo-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 10000
CMD ["java", "-jar", "app.jar"]
